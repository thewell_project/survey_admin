// @flow
import { createAction, handleActions, type ActionType } from 'redux-actions';
import produce from 'immer';
import { applyPenders } from 'redux-pender';
import * as SurveyAPI from 'lib/api/survey';
import axios from 'axios';

const GET_SURVEY_POSTS = 'listing/GET_SURVEY_POSTS';

export const actionCreators = {
  getSurveyDatas: createAction(GET_SURVEY_POSTS, SurveyAPI.getSurveyDatas),
};

export type PostItem = {
  que: string,
  question: string,
  result: ?Array<Array<string>>,
};

export type ListingSet = {
  posts: ?(PostItem[]),
};

export type Listing = {
  surveys: ListingSet,
};

export interface ListingActionCreators {
  getSurveyDatas(): any;
}
type GetResponseAction = {
  type: string,
  payload: {
    data: PostItem[],
  },
  meta: any,
};

const initialListingSet = {
  // posts: null,
  // end: false
};

const initialState: Listing = {
  surveys: initialListingSet,
};

const reducer = handleActions({
}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_SURVEY_POSTS,
    onSuccess: (state: Listing, action: GetResponseAction) => {
      return produce(state, draft => {
        const { data } = action.payload;

        console.log("apply pender", data);
        // action.payload.data.forEach(post => {
        //   draft.trendingMap[post.id] = true;
        // });
        var result = [];
        data.survey.map((e) => {
          result.push(e);  
        });
        //state 값에 저장
        draft.surveys = {
          result
        }
        console.log("draft 값", draft.surveys);
      }); 
    }
  }
]);