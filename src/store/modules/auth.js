import { createAction, handleActions, type ActionType } from 'redux-actions';
import { Record, fromJS, type Map } from 'immutable';
import { pender } from 'redux-pender';

import * as AuthAPI from 'lib/api/auth';
import { string } from 'postcss-selector-parser';

/* ACTION TYPE */
const SET_EMAIL_INPUT = 'auth/SET_EMAIL_INPUT';
const SET_PASSWORD_INPUT = 'auth/SET_PASSWORD_INPUT';
const USER_LOGIN = 'auth/USER_LOGIN';

/* ACTION CREATOR */
export const actionCreators = {
    setEmailInput: createAction(SET_EMAIL_INPUT, (value: string) => value),
    setPasswordInput: createAction(SET_PASSWORD_INPUT, (value: string) => value),
    userLogin: createAction(USER_LOGIN, AuthAPI.userLogin),
};

/* STATE TYPES */
export type Auth = {
    email: string,
    password: string,
    sentEmail: boolean,
    registerForm: {
        displayName: string,
        email: string,
        username: string,
        shortBio: string,
    },
    registerToken: string,
    authResult: AuthResult,
    getData: string,
    surveyData: Array<SurveyResult>,
}

export type AuthResult = ?{
    user: {
      id: string,
      username: string,
      displayName: string,
      thumbnail?: ?string,
    },
    token: string,
  };

export type SurveyResult = ?{
    id: string,
    male: string,
    merry: string,
    age: string,
    job: string,
    first: string,
    second: string,
    third: string,
    fourth: string,
    fifth: string,
    createdAt: string,
    updatedAt: string,
}

export type SurveyItem = {
    id: string,
    male: string,
    merry: string,
    age: string,
    job: string,
    first: string,
    second: string,
    third: string,
    fourth: string,
    fifth: string,
    createdAt: string,
    updatedAt: string,
};

const UserSubRecord = Record({
    id: '',
    username: '',
    displayName: '',
    thumbnail: null,
});

const AuthResultSubRecord = Record({
    user: UserSubRecord(),
    token: '',
});

const SurveyRecord = Record({
    id: '',
    male: '',
    merry: '',
    age: '',
    job: '',
    first: '',
    second: '',
    third: '',
    fourth: '',
    fifth: '',
    createdAt: '',
    updatedAt: '',
});

const AuthRecord = Record(({
    email: '',
    password: '',
    sentEmail: false,
    isUser: false,
    registerForm: {
        displayName: '',
        email: '',
        username: '',
        shortBio: '',
    },
    registerToken: '',
    authResult: null,
    surveyData: null,
}:Auth));

const initialState: Auth = AuthRecord();

export default handleActions({
    [SET_EMAIL_INPUT]: (state, { payload: value }) => {
        return state.set('email', value);
    },
    [SET_PASSWORD_INPUT]: (state, { payload: value }) => {
        return state.set('password', value);
    },
    ...pender({
        type: USER_LOGIN,
        onSuccess: (state, { payload: { data } }) => {
            console.log(data);
            const { user, token } = data; 
            console.log('사용자 로그인 데이터',data);
            return state.set('authResult', AuthResultSubRecord({
                user: UserSubRecord(user),
                token,
            }))
            .set('sentEmail', true).set('isUser', data.isUser);
        },
    }),
}, initialState);