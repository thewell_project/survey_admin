// @flow
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { type ContextRouter } from "react-router-dom";
import { actionCreators as baseActions } from "store/modules/base";
import { actionCreators as authActions } from "store/modules/auth";
import { bindActionCreators, type Dispatch } from "redux";
import { connect } from "react-redux";

import MainTemplate from 'components/templates/MainTemplate';
import MainSidebarContainer from 'containers/main/MainSidebarContainer';
import MainHeadContainer from 'containers/main/MainHeadContainer';
import Temp from './Temp';

type Props = {
  BaseActions: typeof baseActions,
  AuthActions: typeof authActions,
} & ContextRouter;



class Main extends Component<Props> {

  render() {
    return (
      <MainTemplate sidebar={<MainSidebarContainer />}>
        <MainHeadContainer />
        <Temp />
      </MainTemplate>
    );
  }
}

export default Main;
