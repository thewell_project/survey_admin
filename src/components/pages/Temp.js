import React from 'react';
import DashBoardTemplate from '../../components/templates/DashBoardTemplate'
import DashBoardSection from '../../components/sections/DashBoardSection';
import DashBoardCharts from '../../containers/charts/DashBoardCharts';
import { Helmet } from 'react-helmet';

type Props = {};

const Temp = (props: Props) => {
    return (
        <DashBoardTemplate>
            <Helmet>
                <title>설문 현황</title>
                <meta 
                    name="description"
                    content="지금 등록하신 설문의 현황을 도표로 살펴보세요"
                />
            </Helmet>
            <DashBoardSection title="현재 통계 현황">
                {/* 버튼을 누르면 엑셀 출력하기 */}
                <DashBoardCharts />
            </DashBoardSection>
        </DashBoardTemplate>
    )
}

export default Temp;