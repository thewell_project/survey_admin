// @flow
import React, { Component } from "react";
import PageTemplate from "components/templates/PageTemplate";
import LandingTemplateContainer from "containers/landing/LandingTemplateContainer";
import HeaderContainer from "containers/base/HeaderContainer";
import AuthFormContainer from "containers/landing/AuthFormContainer";

import { type ContextRouter } from "react-router-dom";
import { actionCreators as baseActions } from "store/modules/base";
import { actionCreators as authActions } from "store/modules/auth";
import { bindActionCreators, type Dispatch } from "redux";
import { connect } from "react-redux";
import queryString from "query-string";
import PolyBackground from "../base/PolyBackground/PolyBackground";

type Props = {
  BaseActions: typeof baseActions,
  AuthActions: typeof authActions
} & ContextRouter;

class Home extends Component<Props> {
  constructor(props: Props) {
    super(props);
    const { BaseActions, AuthActions } = this.props;
  }

  render() {
    return (
      <PolyBackground>
        {/* <PageTemplate header={<HeaderContainer />}> */}
        <PageTemplate>
          <LandingTemplateContainer form={<AuthFormContainer />} />
        </PageTemplate>
      </PolyBackground>
    );
  }
}

export default connect(
  state => ({}),
  (dispatch: Dispatch<*>) => ({
    BaseActions: bindActionCreators(baseActions, dispatch),
    AuthActions: bindActionCreators(authActions, dispatch)
  })
)(Home);
