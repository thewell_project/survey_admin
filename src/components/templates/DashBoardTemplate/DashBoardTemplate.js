// @flow
import React, { type Node } from 'react';
import './DashBoardTemplate.scss';

type Props = {
  children: Node,
};

const DashBoardTemplate = ({ children }: Props) => (
  <div className="DashBoardTemplate">{children}</div>
);

export default DashBoardTemplate;
