import React from "react";
import Responsive from "components/common/Responsive";
import logo from '../../../static/images/logo.png';
import "./LandingTemplate.scss";


const LandingTemplate = ({ form }) => {
  return (
    <div className="landing-template background-exhibition">
      <Responsive className="block">
        <div className="left-text">
          <div>
            <img src={logo} alt="logo" className="logo-sm" />
            <h1>자료 통계 조사 서비스</h1>
            <div className="description">
              <p>스마트한 행사 설문 분석을 확인해보세요.</p>
              {/* <p>그래프</p> */}
            </div>
          </div>
        </div>
        <div className="right-form">
          <div className="black-box">
            <h2>
              <span className="brand">
                로그인
                {form}
              </span>
            </h2>
          </div>
          <div className="register-button">시작하기</div>
        </div>
      </Responsive>
    </div>
  );
};

export default LandingTemplate;
