// @flow
import React, { type Node } from 'react';
import './StatisticsTemplate.scss';

type Props = {
  children: Node,
};

const StatisticsTemplate = ({ children }: Props) => (
  <div className="StatisticsTemplate">{children}</div>
);

export default StatisticsTemplate;
