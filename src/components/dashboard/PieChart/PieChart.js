import React, { Component } from "react";
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import {
  Card,
  CardHeader,
  CardContent,
  Divider,
  Grid
} from "@material-ui/core";

type Props = {
  id: string,
  number: string,
  question: string,
  result: string
};

class PieChart extends Component<Props> {
  state = {
    legend: []
  };

  componentDidMount() {
    const component = this.doughnut;
    this.setState({ legend: component.chartInstance.legend.legendItems });
  }

  render() {
    const examples = [];
    const rates = [];
    const devices = [];

    console.log("질문들 모음", this.props.question);
    console.log("답변들 부분", this.props.result);

    this.props.result.map(e => {
      rates.push(e[2]);
      examples.push(e[0] + ". " + e[1]);
      devices.push({
        title: e[0] + ". " + e[1],
        value: e[2],
        color: "#F7464A"
      });
    });

    console.log("보기 모음", examples);
    console.log("답변 모음", devices);
    console.log("비율 모음", rates);

    const dataDoughnut = {
      labels: examples,
      datasets: [
        {
          data: rates,
          backgroundColor: [
            "#59CEFC",
            "#F8DE61",
            "#BAF763",
            "#A8BBFB",
            "#FAA960",
            "#77E0AE",
            "#6E7CC8",
            "#FB8197",
            "#4EB0A0",
            "#51619B",
            "#F66A52",
            "#327A8B"
          ],
          hoverBackgroundColor: [
            "#59CEFC",
            "#F8DE61",
            "#BAF763",
            "#A8BBFB",
            "#FAA960",
            "#77E0AE",
            "#6E7CC8",
            "#FB8197",
            "#4EB0A0",
            "#51619B",
            "#F66A52",
            "#327A8B"
          ]
        }
      ]
    };

    const { legend } = this.state;

    // const options = {
    //   legend: {
    //     display: false
    //   },
    //   responsive: true,
    //   maintainAspectRatio: false,
    //   animation: false,
    //   cutoutPercentage: 80,
    //   layout: { padding: 0 },
    //   tooltips: {
    //     enabled: true,
    //     mode: "index",
    //     intersect: false,
    //     borderWidth: 1,
    //     borderColor: "#5AD3D1",
    //     backgroundColor: "#FFFFFF",
    //     titleFontColor: "#FF5A5E",
    //     bodyFontColor: "#5AD3D1",
    //     footerFontColor: "#5AD3D1"
    //   }
    // };
    const listItemStyle = {
      color: "#333",
      listStyle: "none",
      textAlign: "left",
      display: "flex",
      flexDirection: "row",
      margin: "8px"
    };

    return (
      <Card>
        <CardHeader
          title={this.props.number + ". " + this.props.question}
          titleTypographyProps={{ variant: "h6" }}
        />
        <Divider />
        <CardContent
          style={{
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            maxHeight: "388px"
          }}
        >
          <Grid container>
            <Grid
              item
              lg={6}
              sm={6}
              xl={6}
              xs={12}
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <MDBContainer>
                {" "}
                <Pie
                  ref={ref => (this.doughnut = ref)}
                  data={dataDoughnut}
                  options={{
                    responsive: false,
                    title: { display: false },
                    legend: {
                      display: false
                    }
                  }}
                />
              </MDBContainer>
            </Grid>
            <Grid
              item
              lg={6}
              sm={6}
              xl={6}
              xs={12}
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <ul className="mt-8">
                {legend.length &&
                  legend.map(item => {
                    return (
                      <li key={item.text} style={listItemStyle}>
                        <div
                          style={{
                            marginRight: "8px",
                            width: "20px",
                            height: "20px",
                            backgroundColor: item.fillStyle
                          }}
                        />
                        {item.text}
                      </li>
                    );
                  })}
              </ul>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  }
}

export default PieChart;
