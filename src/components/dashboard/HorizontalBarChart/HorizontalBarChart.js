import React from "react";
import { Bar, HorizontalBar } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import clsx from "clsx";
import PropTypes from "prop-types";
import { colors, List, ListItem, ListItemText } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  IconButton,
  Divider,
  Typography,
  Grid
} from "@material-ui/core";
import LaptopMacIcon from "@material-ui/icons/LaptopMac";
import PhoneIphoneIcon from "@material-ui/icons/PhoneIphone";
import RefreshIcon from "@material-ui/icons/Refresh";
import TabletMacIcon from "@material-ui/icons/TabletMac";

import { IoMdMan as ManIcon } from "react-icons/io";
import { IoMdWoman as WomanIcon } from "react-icons/io";

//
type Props = {
  number: string,
  question: string,
  result: ?Array<Array<string>>
};

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%"
  },
  gridContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  gridChart: {
    height: "100%"
  },
  gridStats: {
    height: "100%"
  },
  chartContainer: {
    position: "relative",
    height: "200px"
  },
  mdbContainer: {
    height: "100%",
    display: "flex",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    flex: 1,
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  stats: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  device: {
    textAlign: "center",
    padding: theme.spacing(1)
  },
  deviceIcon: {
    color: theme.palette.icon
  },
  listContainer: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: 200
  },
  listSection: {
    backgroundColor: "inherit"
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0
  }
}));

const BarChart = ({
  id,
  number,
  question,
  result,
  className,
  ...rest
}: Props) => {
  const classes = useStyles();
  const theme = useTheme();

  const examples = [];
  const rates = [];
  const devices = [];

  console.log("질문들 모음", question);
  console.log("답변들 부분", result);

  result.map(e => {
    rates.push(e[2]);
    examples.push(e[0] + ". " + e[1]);
    devices.push({
      title: e[0] + ". " + e[1],
      value: e[2],
      color: theme.palette.primary
    });
  });

  console.log("보기 모음", examples);
  console.log("답변 모음", devices);
  console.log("비율 모음", rates);

  const dataBar = {
      labels: examples,
      datasets: [
        {
          label: "선택 수",
          data: rates,
          backgroundColor: [
            "#DF5360",
            "#F7D971",
            "#F29A65",
            "#F6BEAB",
            "#CBCFD8",
            "#59BDA6",
            "#046C8E",
            "#A08EC2",
            "#B68A82",
            "#48B0CA"
          ],
          borderWidth: 2,
          borderColor: [
            "#DF5360",
            "#F7D971",
            "#F29A65",
            "#F6BEAB",
            "#CBCFD8",
            "#59BDA6",
            "#046C8E",
            "#A08EC2",
            "#B68A82",
            "#48B0CA"
          ]
        }
      ]
    },
    barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 1.0)"
            }
          }
        ],
        yAxes: [
          {
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 1.0)"
            },
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    };

  const options = {
    legend: {
      display: false
    },
    responsive: true,
    maintainAspectRatio: false,
    animation: false,
    cutoutPercentage: 80,
    layout: { padding: 0 },
    tooltips: {
      enabled: true,
      mode: "index",
      intersect: false,
      borderWidth: 1,
      borderColor: colors.grey[200],
      backgroundColor: "#FFFFFF",
      titleFontColor: colors.blueGrey[900],
      bodyFontColor: colors.blueGrey[600],
      footerFontColor: colors.blueGrey[600]
    }
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardHeader
        title={number + ". " + question}
        titleTypographyProps={{ variant: "h6" }}
      />
      <Divider />
      <CardContent>
        <Grid container className={classes.gridContainer}>
          <Grid
            className={classes.gridChart}
            item
            lg={12}
            sm={12}
            xl={12}
            xs={12}
          >
            <div className={classes.chartContainer}>
              <MDBContainer className={classes.mdbContainer}>
                <HorizontalBar 
                  data={dataBar}
                  options={barChartOptions}
                />
                {/* <Bar
                  data={dataBar}
                  options={barChartOptions}
                /> */}
              </MDBContainer>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

BarChart.propTypes = {
  className: PropTypes.string
};

export default BarChart;
