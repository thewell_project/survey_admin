import React from "react";
import { Bar, HorizontalBar } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import clsx from "clsx";
import PropTypes from "prop-types";
import { colors, List, ListItem, ListItemText } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  IconButton,
  Divider,
  Typography,
  Grid
} from "@material-ui/core";
import LaptopMacIcon from "@material-ui/icons/LaptopMac";
import PhoneIphoneIcon from "@material-ui/icons/PhoneIphone";
import RefreshIcon from "@material-ui/icons/Refresh";
import TabletMacIcon from "@material-ui/icons/TabletMac";

import MenuIcon from '@material-ui/icons/Menu';
import InputBase from '@material-ui/core/InputBase';

import { IoMdMan as ManIcon } from "react-icons/io";
import { IoMdWoman as WomanIcon } from "react-icons/io";

//
type Props = {
  number: string,
  question: string,
  result: ?Array<Array<string>>
};

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%"
  },
  cardHeader: {
  },
  cardContent: {
    // height: "100%", 
    display: "flex", 
    alignItems: "center", 
    justifyContent: "center", 
    flexDirection: "row", 
    maxHeight: "388px"
  },
  gridContainer: {
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  gridChart: {
    height: "100%"
  },
  gridStats: {
    height: "100%"
  },
  chartContainer: {
    position: "relative",
    height: "200px"
  },
  mdbContainer: {
    height: "100%",
    display: "flex",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    flex: 1,
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  stats: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  device: {
    textAlign: "center",
    padding: theme.spacing(1)
  },
  deviceIcon: {
    color: theme.palette.icon
  },
  listContainer: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: 200
  },
  listSection: {
    backgroundColor: "inherit"
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0
  },
}));

const BarChart = ({
  id,
  number,
  question,
  result,
  className,
  ...rest
}: Props) => {
  const classes = useStyles();
  const theme = useTheme();

  const examples = [];
  const rates = [];
  const devices = [];

  console.log("질문들 모음", question);
  console.log("답변들 부분", result);

  result.map(e => {
    rates.push(e[2]);
    examples.push(e[0] + ". " + e[1]);
    devices.push({
      title: e[0] + ". " + e[1],
      value: e[2],
      color: theme.palette.primary
    });
  });

  console.log("보기 모음", examples);
  console.log("답변 모음", devices);
  console.log("비율 모음", rates);

  const dataBar = {
      labels: examples,
      datasets: [
        {
          label: "선택 수",
          data: rates,
          backgroundColor: [
            "#59CEFC",
            "#F8DE61",
            "#BAF763",
            "#A8BBFB",
            "#FAA960",
            "#77E0AE",
            "#6E7CC8",
            "#FB8197",
            "#4EB0A0",
            "#51619B",
            "#F66A52",
            "#327A8B"
          ],
          borderWidth: 2,
          borderColor: [
            "#59CEFC",
            "#F8DE61",
            "#BAF763",
            "#A8BBFB",
            "#FAA960",
            "#77E0AE",
            "#6E7CC8",
            "#FB8197",
            "#4EB0A0",
            "#51619B",
            "#F66A52",
            "#327A8B"
          ]
        }
      ]
    },
    barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      label: { display: false },
    };

  const options = {
    title:{
      display: true,
    },
    legend:{
      display: true,
      position: 'right'
    }
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardHeader
        className={classes.cardHeader}
        title={number + ". " + question}
        titleTypographyProps={{ variant: "h6" }}
      >

      </CardHeader>
      <Divider />
      <CardContent className={classes.cardContent}>
        <Grid container className={classes.gridContainer}>
          <Grid
            className={classes.gridChart}
            item
            lg={12}
            sm={12}
            xl={12}
            xs={12}
          >
            <div className={classes.chartContainer}>
              <MDBContainer className={classes.mdbContainer}>
                <Bar 
                  data={dataBar}
                  options={barChartOptions}
                />
                {/* <Bar
                  data={dataBar}
                  options={barChartOptions}
                /> */}
              </MDBContainer>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

BarChart.propTypes = {
  className: PropTypes.string
};

export default BarChart;
