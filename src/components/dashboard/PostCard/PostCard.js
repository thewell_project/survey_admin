// @flow
import React, { Component } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';

import './PostCard.scss'

type Props = {
    title: '',
    data: '',
}

export default class PostCard extends Component<Props> {

    render() {

        const { title, data, icon } = this.props;

        console.log(icon);

        return (
            <Card className="CardForm">
            <CardContent>
              <Grid
                container
                justify="space-between"
              >
                <Grid item>
                  <Typography
                    className="title"
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                  >
                    {title}
                  </Typography>
                  <Typography variant="h5">{data}</Typography>
                </Grid>
                <Grid item>
                  <Avatar className="avatar">
                  {/* <i class="material-icons">
                    {icon}
                  </i> */}
                  </Avatar>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        );
    }
}