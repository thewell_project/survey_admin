import React, { useState } from "react";
import clsx from "clsx";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import PropTypes from "prop-types";
import { colors, List, ListItem, ListItemText } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Card,
  CardActions,
  CardHeader,
  CardContent,
  Divider,
  TableBody,
  TableCell,
  IconButton,
  TableRow,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";
import uuid from "uuid/v1";
import SearchIcon from "@material-ui/icons/Search";

//
type Props = {
  number: string,
  question: string,
  result: ?Array<Array<string>>
};

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%"
  },
  gridContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  gridChart: {
    height: "100%"
  },
  gridStats: {
    height: "100%"
  },
  listContainer: {
    position: "relative",
    width: "100%",
    height: "200px"
  },
  mdbContainer: {
    height: "100%",
    display: "flex",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    flex: 1,
    flexDirection: "row",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  tableContainer: {
    width: "100%"
  },
  stats: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  device: {
    textAlign: "center",
    padding: theme.spacing(1)
  },
  deviceIcon: {
    color: theme.palette.icon
  },
  listContainer: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: 200
  },
  listSection: {
    backgroundColor: "inherit"
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0
  },
  table: {
    width: "100v%"
  }
}));

const ListTable = ({
  id,
  number,
  question,
  result,
  className,
  ...rest
}: Props) => {
  const classes = useStyles();
  const theme = useTheme();

  const examples = [];
  const rates = [];
  const devices = [];
  const datas = [];

  console.log("질문들 모음", question);
  console.log("답변들 부분", result);

  console.log("보기 모음", examples);
  console.log("답변 모음", devices);
  console.log("비율 모음", rates);

  console.log("주관식 부분", result);
  const [open, setOpen] = useState(false);
  let [searchValue] = useState("");

  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] = React.useState([]);

  //result 값 처리
  if (result) {
    console.log("ListTable", searchValue);
    result.map(e => {
      datas.push({
        id: uuid(),
        string: e[0]
      });
    });
  }

  const options = {
    legend: {
      display: false
    },
    responsive: true,
    maintainAspectRatio: false,
    animation: false,
    cutoutPercentage: 80,
    layout: { padding: 0 },
    tooltips: {
      enabled: true,
      mode: "index",
      intersect: false,
      borderWidth: 1,
      borderColor: colors.grey[200],
      backgroundColor: "#FFFFFF",
      titleFontColor: colors.blueGrey[900],
      bodyFontColor: colors.blueGrey[600],
      footerFontColor: colors.blueGrey[600]
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
    console.log("ListTable open", datas);
  };

  React.useEffect(() => {
    const results = datas.filter(data =>
      data.string.toLowerCase().includes(searchTerm)
    );
    setSearchResults(results);
  }, [searchTerm]);

  const handleChange = event => {
    setSearchTerm(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
    console.log("ListTable close", datas);
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardHeader
        title={number + ". " + question}
        titleTypographyProps={{ variant: "h6" }}
        action={
          <IconButton aria-label="settings">
            <SearchIcon onClick={handleClickOpen} />
          </IconButton>
          //Dialog
        }
      />
      <Divider />
      <CardContent style={{ height: "100%" }}>
        <div className={classes.inner}>
          <MDBTable className={classes.tableContainer} scrollY>
            <TableBody>
              {searchResults.map(item => (
                <TableRow hover key={item.id}>
                  <TableCell>{item.string}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </MDBTable>
        </div>
      </CardContent>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">검색</DialogTitle>
        <DialogContent>
          <DialogContentText>검색하시겠어요?</DialogContentText>
          <TextField
            value={searchTerm}
            onChange={handleChange}
            autoFocus
            margin="dense"
            id="name"
            label="검색어"
            type="email"
            fullWidth
          />
          <br />
          <DialogContentText>
            {/* {searchTerm && "일치하는 소원 수 : " + searchResults.length} */}
            {searchTerm && (
              <MDBTable className={classes.tableContainer} scrollY>
                <TableBody>
                  {searchResults.map(item => (
                    <TableRow hover key={item.id}>
                      <TableCell>{item.string}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </MDBTable>
            )}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            닫기
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
};

ListTable.propTypes = {
  className: PropTypes.string
};

export default ListTable;
