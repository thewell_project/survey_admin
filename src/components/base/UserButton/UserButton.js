// @flow
import React from 'react';
import menu from 'static/images/menu.png';
import defaultThumbnail from 'static/images/default_thumbnail.png';
import './UserButton.scss';

type Props = {
    onClick(): void,
    thumbnail: ?string,
};

const UserButton = ({ onClick, thumbnail }: Props) => {
  return (
    <div className="UserButton">
      <div className="thumbnail" onClick={onClick}>
        <img src={thumbnail || menu} alt="thumbnail" />
      </div>
    </div>
  );
};

export default UserButton;