// @flow
import React from 'react';
import withClickOutside from 'react-onclickoutside';
import { Link } from 'react-router-dom';
import UserMenuItem from 'components/base/UserMenuItem';
import './UserMenu.scss';

type Props = {
  onExportExcel(): void,
  onClick(): void,
  onLogout(): Promise<*>,
  username: string,
};

const UserMenu = ({ onExportExcel, onClick, onLogout, username }: Props) => {
  return (
    <div className="user-menu-wrapper">
      <div className="user-menu-positioner">
        <div className="user-menu" onClick={onClick}>
          <div className="menu-items">
            {/* <UserMenuItem to={`/dashboard`}>통계</UserMenuItem>
            <div className="separator" />
            <UserMenuItem to="/settings">설정</UserMenuItem> */}
            <UserMenuItem onClick={onLogout}>로그아웃</UserMenuItem>
            <div className="separator" />
            <UserMenuItem onClick={onExportExcel}>엑셀 내보내기</UserMenuItem>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserMenu;