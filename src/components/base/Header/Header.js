// @flow
import React, { type Node } from 'react';
import Responsive from 'components/common/Responsive';
import './Header.scss';

type Props = {
    right: Node,
    userMenu: Node,
};

const Header = ({ right, userMenu }: Props) => {
    return (
        <header className="base header">
            <Responsive className="header-wrapper">
                <div className="brand">
                    Smart Survey
                </div>
                <nav>
                    {/* <a className="active" href="/">서비스 소개</a> */}
                </nav>
                <div className="right">
                    {right}
                </div>
                {userMenu}
            </Responsive>
        </header>
    );
};

export default Header;