// @flow
import React, { Fragment, type Node } from 'react';
import Button from 'components/common/Button';
import type { UserData } from 'store/modules/user';
import { Link } from 'react-router-dom';

import './MainHead.scss';

type Props = {
  onLogin(): void,
  rightArea: ?Node,
  logged: boolean,
};

const MainHead = ({ logged, onLogin, rightArea }: Props) => (
  <div className="MainHead">
    <div className="button-area">
    </div>
    <div className="spacer" />
    <Link to="/" className="mobile-logo">
      THEWELL
    </Link>
    <div className="right-area">
      {rightArea || (
        <Button theme="outline" onClick={onLogin}>
          <i class="material-icons">
            menu
          </i>
        </Button>
      )}
    </div>
  </div>
);

export default MainHead;
