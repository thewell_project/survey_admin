// @flow
import React, { Component, type Node } from "react";
import DashboardIcon from "@material-ui/icons/Dashboard";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import SettingsIcon from "@material-ui/icons/Settings";

import { Link, withRouter, type Match } from "react-router-dom";
import { Avatar, Typography } from "@material-ui/core";
import MainMenuItem from "components/main/MainMenuItem";

import logo from "../../../static/images/logo.png";
import surveyProfile from "../../../static/images/survey_profile.png";
import "./MainSidebar.scss";

type Props = {
  url: string
};

class MainSidebar extends Component<Props> {
  render() {
    const { url } = this.props;

    return (
      <aside className="MainSidebar" style={{ backgroundColor: "#d0ebff" }}>
        <div className="banner">
        <img src={logo} className="logo" alt="logo" />
        </div>
        <ul className="menu">
          {/* 프로필 부분 넣기 */}
          <div className="profile">
            <Avatar
              alt="Person"
              className="avatar"
              src={surveyProfile}
              to="/settings"
            />
            {/* bofLogo.png로 교체해주세요. */}
            <Typography className="typography">
              2019<br/>강소기업<br/>육성사업<br/>
            </Typography>
          </div>
          <MainMenuItem
            icon={<DashboardIcon />}
            text="통계"
            active={["/", "/trending"].indexOf(url) > -1}
            to="/main"
          />
          {/* <MainMenuItem
            active={/^\/tags/.test(url)}
            icon={<SettingsIcon />}
            text="설정"
            to="/settings"
          /> */}
        </ul>
        <div className="placer" />
      </aside>
    );
  }
}

export default MainSidebar;
