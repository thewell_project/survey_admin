import React, { Component } from "react";
import SocialLoginButton from "components/landing/SocialLoginButton";
import { IoIosCheckmark } from "react-icons/io";
import Spinner from "components/common/Spinner";
import cx from "classnames";
import "./AuthForm.scss";

type Props = {
  onChange(e: Event): void,
  onPassword(e: Event): void,
  onSendVerification(): Promise<*>,
  onEnterKeyPress(e: KeyboardEvent): void,
  email: string,
  password: string,
  sentEmail: boolean,
  sending: boolean,
  isUser: boolean
};

const AuthForm = ({
  onChange,
  onPassword,
  onSendVerification,
  onEnterKeyPress,
  email,
  password,
  sentEmail,
  sending,
  isUser
}: Props) => {
  return (
    <div className="auth-form">
      {sentEmail ? (
        //   여기서 그럼 로그인 화면 처리하면 되는건가
        <div className="sent-email">
          <IoIosCheckmark />
          <div className="text">
            {isUser ? "로그인" : "회원가입"} 링크가 이메일로 전송되었습니다.
            <br />
            이메일의 링크를 통하여 회원가입을 계속하세요.
          </div>
        </div>
      ) : (
        <div className={cx("input-with-button", { sending })}>
          <input
            placeholder="이메일을 입력해주세요"
            value={email}
            onChange={onChange}
            disabled={sending}
          />
          <input
            placeholder="비밀번호를 입력해주세요"
            type="password"
            value={password}
            onChange={onPassword}
            disabled={sending}
            onKeyPress={onEnterKeyPress}
          />

          <div className="seperator">
            <div className="or"></div>
          </div>

          <div className="button" 
            onClick={onSendVerification}>
            {sending ? <Spinner size="3rem" /> : "시작"}
          </div>
          {/* <div className="button" onClick={onSendVerification}>
            {sending ? <Spinner size="3rem" /> : "시작하기"}
          </div> */}
        </div>
      )}
      {/* <div className="seperator">
        <div className="or"></div>
      </div> */}
      <div className="social-buttons">
        {/* <SocialLoginButton type="google" /> */}
      </div>
    </div>
  );
};

export default AuthForm;
