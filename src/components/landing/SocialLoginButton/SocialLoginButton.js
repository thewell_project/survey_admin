// @flow
import React from 'react';
import cx from 'classnames';
// import GoogleIcon from 'react-icons/lib/fa/google';
import { IoLogoGoogleplus } from "react-icons/io";
import './SocialLoginButton.scss';

type Props = {
    type: 'google'
}

const providers = {
    google: {
        icon: IoLogoGoogleplus,
    }
}

const SocialLoginButton = (props: Props) => {
    const { type } = props;
    const { icon: Icon } = providers[type];

    console.log( type );

    return (
        <div className={cx('social-login-button', type)}>
            {/* <div className="icon">
                <Icon />
            </div> */}
            <div className="text">
                {/* { type }  */}
                <span className="login">시작하기</span>
            </div>
        </div>
    )
}

SocialLoginButton.defaultProps = {
    type: 'github',
}

export default SocialLoginButton;