// @flow
import React, { Component, type Node } from "react";
import { Button } from "react-bootstrap";
import type { State } from "store";
import { connect } from "react-redux";

import PrintIcon from '@material-ui/icons/Print';

import "./DashBoardSection.scss";

type Props = {
  title: string,
  children: Node,
  excelData: null
};

class DashBoardSection extends Component<Props> {
  
  componentDidUpdate() {

  }

  render() {
    const { title, children } = this.props;
    const { excelData } = this.props;
    console.log("값이 들어옴", excelData);

    return (
      <div className="DashBoardSection">
        {/* <h2>{title}</h2> */}
        {/* <PrintIcon 
          onClick={this.exportExcel}
          style={{ marginLeft: "30px" }}
        /> */}
        <div className="contents">{children}</div>
      </div>
    );
  }
}

export default connect(
  ({ listing }: State) => ({
    excelData: listing.surveys.result
  }),
  () => ({})
)(DashBoardSection);
