import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";
import PieChart from '../dashboard/TestChart';

const AntTabs = withStyles({
  root: {
    flexGrow: 1,
    borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs);

function TabPanel(props) {
    const { result, value, index, ...other } = props;
      
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`nav-tabpanel-${index}`}
        height="200px"
        aria-labelledby={`nav-tab-${index}`}
        {...other}
      >
        <Box p={1}>
          <PieChart 
              result={result}
          />
        </Box>
      </Typography>
    );
}

const AntTab = withStyles(theme => ({
  root: {
    flexGrow: 1,
    textTransform: 'none',
    minWidth: 72,
    fontWeight: 900,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      color: '#40a9ff',
      opacity: 1,
    },
    '&$selected': {
      color: '#1890ff',
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: '#40a9ff',
    },
  },
  selected: {},
}))(props => <Tab disableRipple {...props} />);

const StyledTabs = withStyles({
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    '& > div': {
    //   maxWidth: 40,
      width: '100%',
      backgroundColor: '#635ee7',
    },
  },
})(props => <Tabs {...props} TabIndicatorProps={{ children: <div /> }} />);

const StyledTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    color: '#fff',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    marginRight: theme.spacing(1),
    '&:focus': {
      opacity: 1,
    },
  },
}))(props => <Tab disableRipple {...props} />);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
  },
  demo2: {
    backgroundColor: '#2e1534',
  },
}));

type Props = {
    data: ""
};

const TestSection = ({ data }: Props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <div className={classes.demo1}>
        <AntTabs value={value} onChange={handleChange} aria-label="ant example">
          <AntTab label="설문 항목 1" />
          <AntTab label="설문 항목 2" />
          <AntTab label="설문 항목 3" />
          <AntTab label="설문 항목 4" />
          <AntTab label="설문 항목 5" />
        </AntTabs>
        <Typography className={classes.padding} />
        <TabPanel value={value} index={0} result={data[0]} />
        <TabPanel value={value} index={1} result={data[1]} />
        <TabPanel value={value} index={2} result={data[2]} />
        <TabPanel value={value} index={3} result={data[3]} />
        <TabPanel value={value} index={4} result={data[4]} />
      </div>
    </div>
  );
}

export default TestSection;