// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

// import PieChart from '../dashboard/PieChart';
import PieChart from '../dashboard/TestChart';

type Props = {
  data: ""
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function TabPanel(props) {
  const { result, value, index, ...other } = props;
    
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      <Box p={3}>
        <PieChart 
            result={result}
        />
      </Box>
    </Typography>
  );
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={event => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  }
}));

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`
  };
}

const TabsSection = ({ data }: Props) => {

  console.log('여기까지는 오나요',data);

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    console.log(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
          aria-label="nav tabs example"
        >
          <LinkTab label="질문 1" href="/drafts" {...a11yProps(0)} />
          <LinkTab label="질문 2" href="/trash" {...a11yProps(1)} />
          <LinkTab label="질문 3" href="/spam" {...a11yProps(2)} />
          <LinkTab label="질문 4" href="/spam" {...a11yProps(3)} />
          <LinkTab label="질문 5" href="/spam" {...a11yProps(4)} />
        </Tabs>
      </AppBar>

      <TabPanel value={value} index={0} result={data[0]} />
      <TabPanel value={value} index={1} result={data[1]} />
      <TabPanel value={value} index={2} result={data[2]} />
      <TabPanel value={value} index={3} result={data[3]} />
      <TabPanel value={value} index={4} result={data[4]} />
    </div>
  );
};

export default TabsSection;
