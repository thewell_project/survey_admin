import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home, Register, Main } from "components/pages";
import Callback from "containers/etc/Callback";

import Core from 'containers/etc/Core';

const App = () => (
    <React.Fragment>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/register" component={Register} />
        <Route path="/main" component={Main} />
      </Switch>
      <Core />
    </React.Fragment>
);

//flow 설정이 필요해 보임
//패스 설정 관련
//1시간 6분

//export path = []

export default App;
