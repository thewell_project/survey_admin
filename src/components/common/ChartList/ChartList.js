// @flow
import React from "react";
import type { PostItem } from "store/modules/auth";
import { Grid } from "@material-ui/core";
import { Table } from "react-bootstrap";

//components
//type 1인 객관식인 경우
// import BarChart from "components/dashboard/BarChart";
import BarChart from "components/dashboard/BarChart";
import PieChart from "components/dashboard/PieChart";
import DoughnutChart from "components/dashboard/DoughnutChart";
import uuid from "uuid/v1";
//type 2인 주관식인 경우
import ListTable from "components/dashboard/ListTable";

import "./ChartList.scss";

type Props = {
  results: ?(PostItem[]),
  loading: Boolean
};

const ChartList = ({ 
  results, 
  loading 
}: Props) => {
  //값이 없다하면 return null;
  if (!results) return null;

  console.log("ChartList 부분", results);

  //PieChart 값에 필요한 데이터 전달
  //Data 구조 파악
  const chartList = results.map(e => {
    //결과값이 있는 경우만 반환
    if (e.result !== null) {
      //객관식일때
      if (e.type === "1") {
        //kind 1일때 BarChart
        //kind 2일때 PieChart
        //kind 3일때 DoughnutChart
        if (e.kind === "1") {
          return (
            <Grid item lg={6} sm={12} xl={5} xs={12}>
              <BarChart
                key={e.id}
                number={e.id}
                question={e.question}
                result={e.result}
              />
            </Grid>
          );
        }
        if (e.kind === "2") {
          return (
            <Grid item lg={6} sm={12} xl={5} xs={12}>
              <PieChart
                key={e.id}
                number={e.id}
                question={e.question}
                result={e.result}
              />
            </Grid>
          );
        }
        if (e.kind === "3") {
          return (
            <Grid item lg={6} sm={12} xl={5} xs={12}>
              <DoughnutChart
                key={e.id}
                number={e.id}
                question={e.question}
                result={e.result}
              />
            </Grid>
          );
        }
      }
      // 주관식일때
      if (e.type === "2") {
        return (
          <Grid item lg={6} sm={12} xl={5} xs={12}>
            <ListTable
              key={e.id}
              number={e.id}
              question={e.question}
              result={e.result}
            />
          </Grid>
        );
      }
    }
  });
  console.log("ChartList 부분 ", loading);
  if (loading) {
    return (
      <div>
        <h2>불러오는 중..</h2>
      </div>
    )
  }

  return (
    <div>
      <Grid container spacing={6} className="GridContainer">
        {chartList}
      </Grid>
    </div>
  );
};

ChartList.defaultProps = {
  results: []
};

export default ChartList;
