// @flow
import { pender } from "redux-pender";
import type { $AxiosXHR, $AxiosError } from "axios";

export const pressedEnter = (fn: () => void) => (e: KeyboardEvent) => {
  if (e.key === "Enter") {
    fn();
  }
  return null;
};

type Reducer = (state: any, action: any) => any;

export function applyPenders<T: Reducer>(reducer: T, penders: any[]): T {
  const updaters = Object.assign({}, ...penders.map(pender));
  return ((state, action) => {
    if (updaters[action.type]) {
      return updaters[action.type](state, action);
    }
    return reducer(state, action);
  }: any);
}

export type ResponseAction = {
  type: string,
  payload: $AxiosXHR<*>,
  error: $AxiosError<*>,
  meta: any
};

export type GenericResponseAction<D, M = any> = {
  type: string,
  payload: {
    data: D
  },
  meta: M
};

type Return_<R, F: (...args: Array<any>) => R> = R;
export type Return<T> = Return_<*, T>;

export function loadScript(url: string) {
  return new Promise((resolve, reject) => {
    const script = document.createElement("script");
    script.onload = function onload() {
      resolve();
    };
    script.onerror = function onerror() {
      reject();
    };
    script.src = url;
    if (!document || !document.head) return;
    document.head.appendChild(script);
  });
}
