// @flow

import axios, { type Axios } from 'axios';

axios.defaults.withCredentials = false;

const baseURL = 'https://1fw6qleo0j.execute-api.us-east-1.amazonaws.com/dev/';

const defaultClient: Axios = axios.create({
  baseURL,
  withCredentials: false,
});

defaultClient.defaults.timeout = 5000;

export default defaultClient;
