// @flow
import axios from 'lib/defaultClient';
import queryString from 'query-string';

export const getSurveyDatas = (): Promise<*> => axios.post('/survey/getSurveyData');