// @flow
import axios from 'lib/defaultClient';

export const userLogin = (email: string, password: string): Promise<*> => axios.post('/auth/user-login', { email, password });
export const check = (): Promise<*> => axios.get('/auth/check');
export const logout = (): Promise<*> => axios.get('/auth/logout');