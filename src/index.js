import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import './styles/main.scss';
import * as serviceWorker from './serviceWorker';
import axios from 'axios';

ReactDOM.render(<Root />, document.getElementById('root'));

if (process.env.NODE_ENV === 'production') {
    axios.defaults.baseURL = 'https://1fw6qleo0j.execute-api.us-east-1.amazonaws.com/dev/';
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
