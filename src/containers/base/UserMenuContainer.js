// @flow
import React, { Component } from "react";
import UserMenu from "components/base/UserMenu";
import { connect } from "react-redux";
import type { State } from "store";
import { BaseActions, UserActions } from "store/actionCreators";
import storage, { keys } from "lib/storage";
import TableToExcel from "@linways/table-to-excel";

type Props = {
  visible: boolean,
  username: string,
  excelData: null
};

class UserMenuContainer extends Component<Props> {
  onClickOutside = e => {
    const { visible } = this.props;
    console.log("visible value", visible);
    if (!visible) {
      return null;
    }
    BaseActions.hideUserMenu();
  };

  onClick = () => {
    BaseActions.hideUserMenu();
  };

  onLogout = async () => {
    console.log("로그아웃 부분", keys.user);
    try {
      await UserActions.logout();
    } catch (e) {
      console.log(e);
    }
    storage.remove(keys.user);
    window.location.href = "/";
  };

  onExportExcel = () => {
    TableToExcel.convert(document.getElementById("table-to-xls"), {
      name: "성과보고회 결과 파일.xlsx",
      sheet: {
        name: "Sheet 1"
      }
    });
  }

  temp = value => {
    const { excelData } = this.props;
    console.log("여기서 보자", excelData);
    if (value) {
      console.log("안쪽 값", value);
      console.log("안쪽 값", value.result);
      //값이 있을때만 작업
      //객관식, 주관식으로 나눠서 작업

      //객관식일때
      if (value.type === "1") {
        return (
          <div>
            <tr style={{ borderLeft: "3px solid", borderTop: "3px solid", borderRight: "3px solid" }}>
              <td colSpan="2"></td>
              {value.result.map(e => {
                return <td colSpan="1">{e[1]}</td>;
              })}
            </tr>
            <tr style={{ borderLeft: "3px solid", borderBottom: "3px solid", borderRight: "3px solid" }}>
              <td colSpan="1">{value.id}</td>
              <td colSpan="1">{value.question}</td>
              {value.result.map(e => {
                return <td colSpan="1">{e[2]}</td>;
              })}
            </tr>
          </div>
        );
      }

      //주관식일때
      if (value.type === "2") {
        return (
          <div>
            <tr>
              <td colSpan="1">{value.id}</td>
              <td colSpan="1">{value.question}</td>
            </tr>
            {value.result.map(e => {
              return (
                <tr>
                  <td colSpan="7">{e[0]}</td>
                </tr>
              );
            })}
          </div>
        );
      }
    }
  };

  render() {
    const { visible, username, excelData } = this.props;
    const { onClickOutside, onExportExcel, onClick, onLogout } = this;
    if (!visible) return null;

    return (
      <div>
        <UserMenu
          username={username}
          onClickOutside={onClickOutside}
          onClick={onClick}
          onLogout={onLogout}
          onExportExcel={onExportExcel}
          eventTypes={["click", "touchend"]}
        />
        <div 
          style={{ display: "none" }}
        >
          <table id="table-to-xls" style={{ border: "1px solid #444444" }}>
            <tbody>{excelData && excelData.map(this.temp)}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ base, user, listing }: State) => ({
    visible: base.userMenu,
    username: user.user && user.user.username,
    excelData: listing.surveys.result
  }),
  () => ({})
)(UserMenuContainer);
