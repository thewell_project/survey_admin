import React, { Component, type Node } from 'react';
import Header from 'components/base/Header';
import withUser from 'lib/hoc/withUser';
import type { UserData } from 'store/modules/user';
import UserButtonContainer from './UserButtonContainer';
import UserMenuContainer from './UserMenuContainer';

type Props = {
    user: ?UserData
};

class HeaderContainer extends Component<Props> {
    renderRight(): Node {
        const { user } = this.props;
        if (!user) {
            return (<div>THEWELL</div>);
        }
        return (<UserButtonContainer />);
    }
    render() {
        return (
            <Header
                right={this.renderRight()}
                userMenu={<UserMenuContainer eventTypes={['click', 'touched']}/>}
            />
        )
    }
}

export default withUser(HeaderContainer);