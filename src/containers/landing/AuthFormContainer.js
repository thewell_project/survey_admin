import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AuthActions, UserActions } from 'store/actionCreators';
import type { State } from 'store';
import type { AuthResult } from 'store/modules/auth';
import AuthForm from 'components/landing/AuthForm';
import { pressedEnter } from 'lib/common';
import storage, { keys } from 'lib/storage';
import { withRouter } from "react-router-dom";

type Props = {
    email: string,
    password: string,
    sentEmail: boolean,
    sending: boolean,
    isUser: boolean,
    authResult: AuthResult,
    history: RouterHistory,
}

class AuthFormContainer extends Component<Props> {

    onEnterKeyPress = pressedEnter(() => {
        this.onSendVerification();
    })

    onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
        const { value } = e.target;
        AuthActions.setEmailInput(value);
    }

    onPassword = (e: SyntheticInputEvent<HTMLInputElement>) => {
        const { value } = e.target;
        AuthActions.setPasswordInput(value);
    }

    onSendVerification = async(): Promise<*> => {
        
        const { email, password } = this.props;
        try {
            await AuthActions.userLogin(email, password);
            const { authResult } = this.props;
            console.log('안쪽 값', authResult);
            if (!authResult) return;
            const { token, user } = authResult;
      
            UserActions.setUser(user);
            storage.set(keys.user, user);
            
            const { history } = this.props;
            history.push('/main'); 

        }catch(e) {
            console.log(e);
        }
    }

    render() {
        const { onChange, onPassword, onSendVerification, onEnterKeyPress } = this;
        const { email, password, sentEmail, sending, isUser } = this.props;

        return (
            <div>
                <AuthForm 
                    isUser={isUser}
                    email={email}
                    password={password}
                    sending={sending}
                    sentEmail={sentEmail}
                    onChange={onChange}
                    onPassword={onPassword}
                    onSendVerification={onSendVerification}
                    onEnterKeyPress={onEnterKeyPress}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    email: state.auth.email,
    password: state.auth.password,
})

export default connect(
    ({ auth, pender }: State) => ({
        email: auth.email,
        password: auth.password,
        sentEmail: auth.sentEmail,
        isUser: auth.isUser,
        sending: pender.pending['auth/USER_LOGIN'],
        authResult: auth.authResult,
    }),
    () => ({}),
)(withRouter(AuthFormContainer));