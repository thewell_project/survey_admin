import React, { Component } from "react";
import type { State } from "store";
import { ListingActions } from "store/actionCreators";
import type { PostItem } from "store/modules/auth";
import { connect } from "react-redux";
import axios from 'axios';

import ChartList from "components/common/ChartList";

type Props = {
  results: ?(PostItem[]),
  loading: boolean
};

class DashBoardCharts extends Component<Props> {
  //값을 옮기기
  prefetch = async () => {
    const { results, loading } = this.props;
    // console.log("여기서 조회", loading);
    // console.log("가져온 값 state 부분", results);
  };

  //데이터 값을 받고
  initialize = async() => {
    try {
      await ListingActions.getSurveyDatas();
      this.prefetch();
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    this.initialize();
  }

  render() {
    return (
      <div style={{ padding: 24 }}>
        <ChartList loading={this.props.loading} results={this.props.results} />
      </div>
    );
  }
}

//state 값과 props랑 연결
const mapStateToProps = ({ listing, pender }: State) => ({
  results: listing.surveys.result,
  loading: pender.pending["listing/GET_TRENDING_POSTS"]
});

//react-redux connect 부분
export default connect(mapStateToProps, () => ({}))(DashBoardCharts);
