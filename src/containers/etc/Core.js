// @flow
import React, { Component } from 'react';
import { UserActions } from 'store/actionCreators';
import type { State } from "store";
import type { UserData } from 'store/modules/user';
import { connect } from 'react-redux';
import storage from 'lib/storage';
import { withRouter } from 'react-router-dom';
import { type Location, type RouterHistory } from 'react-router-dom';

type Props = {
    user: ?UserData,
    history: RouterHistory,
};

class Core extends Component<Props> {
    checkUser = async () => {
        const storedUser = storage.get('__survey_user__');
        if (!storedUser) {
          console.log("Core 부분 유저 정보가 저장된 경우");
          UserActions.processUser();
          return;
        }
        UserActions.setUser(storedUser);
        const { history } = this.props;
        // history.push('/');
        console.log("유저 체크해보기");
        history.push('/main');
        // try {
        //   await UserActions.checkUser();
        // } catch (e) {
        //   storage.remove('__survey_user__');
        // }
    };
    initialize = async () => {
        this.checkUser();
    }

    componentDidMount() {
        this.initialize();
    }

    render() {
        return (
            <div />
        );
    }
}

export default connect(
  ({ user }: State) => ({
    user: user.user,
  }),
  () => ({}),
)(withRouter(Core));